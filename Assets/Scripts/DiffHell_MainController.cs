﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiffHell_MainController : MonoBehaviour {

    private void Start()
    {
        int r;
        int.TryParse("5",out r);
        Debug.Log(r.ToString());
    }
    public GameObject G;
    public GameObject N;
    public GameObject X;
    public GameObject Y;

    public Animator[] stage1;
    public GameObject[] stage2;
    public Animator[] stage3;


    public void StartDiffHell()
    {
        int g = int.Parse(G.transform.Find("InputField").GetComponent<InputField>().text);
        int n = int.Parse(N.transform.Find("InputField").GetComponent<InputField>().text);
        int x = int.Parse(X.transform.Find("InputField").GetComponent<InputField>().text);
        int y = int.Parse(Y.transform.Find("InputField").GetComponent<InputField>().text);
        

        int k1 = getPower(g, x, n);
        int k2 = getPower(g, y, n);
        {
            int t = k1;
            k1 = getPower(k2, x, n);
            k2 = getPower(t, y, n);
        }
    }
    public int getPower(int x,int p, int m)
    {
        int r = x;
        for (int i = 1; i < p; i++)
        {
            r = r*x;
            r = r % m;
        }
        return r;
    }
    public int getInverse(int x, int m )
    {
        int r = ModularInverse.Inverse(x, m);
        Debug.Log(r.ToString());
        return r;
    }
}
