﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModularInverse{
    public static int Inverse ( int a, int b)
    {
        int b0 = b;
        int t;
        int q;
        int x0=0;
        int x1 = 1;
        
        if (b == 1)
            return 1;

        while (a > 1)
        {
            q = a / b;
            t = b;
            b = a % b;
            a = t;
            t = x0;
            x0 = x1 - q * x0;
            x1 = t;
        }

        if (x1 < 0)
            x1 += b0;
        return x1;
    }
}
